package com.mpolitakis.leaderboard.repository


import com.mpolitakis.leaderboard.networking.response.LeaderBoardResponseItem


interface LeaderBoardRepository {

        suspend fun getLeaderBoard(page : Int) : List<LeaderBoardResponseItem>

}