package com.mpolitakis.leaderboard.repository


import com.mpolitakis.leaderboard.networking.ApiService
import com.mpolitakis.leaderboard.networking.response.LeaderBoardResponseItem
import javax.inject.Inject


class LeaderBoardRepositoryImpl @Inject constructor(
                                                    private val api: ApiService
): LeaderBoardRepository {


   override suspend fun getLeaderBoard(page: Int) : List<LeaderBoardResponseItem>{

           return api.getLeaderBoard(page)

    }


}