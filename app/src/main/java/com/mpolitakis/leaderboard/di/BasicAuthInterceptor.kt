package com.mpolitakis.leaderboard.di

import dagger.Module
import dagger.Provides
import dagger.hilt.InstallIn
import dagger.hilt.components.SingletonComponent
import okhttp3.*
import java.io.IOException
import javax.inject.Inject
import javax.inject.Singleton
import android.net.NetworkInfo

import android.net.ConnectivityManager
import android.util.Log


class BasicAuthInterceptor@Inject constructor(user: String, password: String) :
    Interceptor {
    private val credentials: String = Credentials.basic(user, password)

    @Throws(IOException::class)
    override fun intercept(chain: Interceptor.Chain): Response {
        val request: Request = chain.request()
        val authenticatedRequest = request.newBuilder()
            .header("Authorization", credentials).build()
        try {
            return chain.proceed(authenticatedRequest)
        }catch (e: Throwable) {
            Log.e("My Connectivity" , "No connection")
            if (e is IOException) {
                Log.e("If IOException=>%s", e.toString())
                throw e
            } else {
                Log.e("Else IOException=>%s", e.toString())
                throw IOException(e)
            }
        }


    }


}
