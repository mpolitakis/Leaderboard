package com.mpolitakis.leaderboard.ui.main

import android.view.LayoutInflater
import android.view.ViewGroup
import androidx.recyclerview.widget.RecyclerView
import com.mpolitakis.leaderboard.R
import com.mpolitakis.leaderboard.databinding.ItemLeaderboardBinding
import com.mpolitakis.leaderboard.networking.response.LeaderBoardResponseItem
import com.squareup.picasso.Picasso
import jp.wasabeef.picasso.transformations.CropCircleTransformation


class RecyclerViewAdapter(private var players : List<LeaderBoardResponseItem>) :
    RecyclerView.Adapter<RecyclerViewAdapter.ViewHolder>() {


    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): ViewHolder {
        val layoutInflater = LayoutInflater.from(parent.context)
        val binding = ItemLeaderboardBinding.inflate(layoutInflater, parent, false)
        return ViewHolder(binding )
    }

    override fun onBindViewHolder(holder: ViewHolder, position: Int) {
        holder.bind(players[position], position)
    }

    override fun getItemCount(): Int = players.size

    class ViewHolder(private val binding: ItemLeaderboardBinding) :
        RecyclerView.ViewHolder(binding.root) {

        fun bind(player: LeaderBoardResponseItem, position: Int) {

            binding.leaderBoardNameDisplay.text = player.name
            Picasso.get().load(player.avatar).transform(CropCircleTransformation()).into(binding.leaderBoardIconDisplay)
            binding.leaderBoardScoreDisplay.text = player.points.toString()
            binding.leaderBoardPlaceDisplay.text = position.toString()
            binding.leaderBoardStarDisplay.setImageResource(R.drawable.bronze_star)
        }

    }

    public fun refreshData(players: List<LeaderBoardResponseItem>){
        this.players = players
        notifyDataSetChanged()
    }



}