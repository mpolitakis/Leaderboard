package com.mpolitakis.leaderboard.ui.main

import androidx.lifecycle.LiveData
import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.ViewModel
import androidx.lifecycle.viewModelScope
import com.mpolitakis.leaderboard.networking.response.LeaderBoardResponseItem
import com.mpolitakis.leaderboard.repository.LeaderBoardRepository
import dagger.hilt.android.lifecycle.HiltViewModel
import kotlinx.coroutines.launch
import javax.inject.Inject



@HiltViewModel
class MainViewModel @Inject constructor(private val repository : LeaderBoardRepository) : ViewModel() {

    private val _leaderboard = MutableLiveData<List<LeaderBoardResponseItem>>()
    val leaderboard: LiveData<List<LeaderBoardResponseItem>>
        get() = _leaderboard

    fun getList(page: Int) {
        viewModelScope.launch {
            _leaderboard.value = repository.getLeaderBoard(page)
        }
    }

}