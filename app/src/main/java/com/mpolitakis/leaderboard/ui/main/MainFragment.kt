package com.mpolitakis.leaderboard.ui.main

import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.view.animation.AlphaAnimation
import android.widget.ImageButton
import androidx.databinding.DataBindingUtil
import androidx.fragment.app.Fragment
import androidx.lifecycle.ViewModelProvider
import androidx.lifecycle.lifecycleScope
import androidx.recyclerview.widget.LinearLayoutManager
import com.mpolitakis.leaderboard.R
import com.mpolitakis.leaderboard.databinding.MainFragmentBinding
import com.mpolitakis.leaderboard.networking.response.LeaderBoardResponseItem
import dagger.hilt.android.AndroidEntryPoint
import kotlinx.coroutines.launch


@AndroidEntryPoint
class MainFragment : Fragment() {

    companion object {
        fun newInstance() = MainFragment()
    }

    private lateinit var viewModel: MainViewModel
    private val adapter by lazy { RecyclerViewAdapter(emptyList()) }
    private lateinit var binding: MainFragmentBinding
    private var leaderboardView = mutableListOf<LeaderBoardResponseItem>()
    private lateinit var too : ImageButton
    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View {


        binding = DataBindingUtil.inflate(inflater, R.layout.main_fragment, container, false)

        binding.lifecycleOwner = this
        too = binding.refresh

        too.setOnClickListener{
            adapter.refreshData(emptyList())
            adapter.notifyDataSetChanged()
        }

        setHasOptionsMenu(true)
        return binding.root

    }

    override fun onActivityCreated(savedInstanceState: Bundle?) {
        super.onActivityCreated(savedInstanceState)

        viewModel = ViewModelProvider(this).get(MainViewModel::class.java)

        viewModel.getList(0)
        observeLeaderBoard()
        setupRecycleView()



    }



    private fun observeLeaderBoard() {
        viewModel.leaderboard.observe(viewLifecycleOwner, {

                    lifecycleScope.launch {
                        leaderboardView.addAll(it)
                        adapter.refreshData(leaderboardView) }

    })
    }
    private fun setupRecycleView() {
        val layoutManager = LinearLayoutManager(context)
        val recyclerView = binding.recyclerView
        recyclerView.layoutManager = layoutManager
        recyclerView.adapter = adapter





        binding.recyclerView.addOnScrollListener(object : EndlessRecyclerOnScrollListener(layoutManager) {
            override fun onLoadMore(current_page: Int) {
                viewModel.getList(current_page)
            }

        })


    }
    private val buttonClick = AlphaAnimation(1f, 0.8f)





}