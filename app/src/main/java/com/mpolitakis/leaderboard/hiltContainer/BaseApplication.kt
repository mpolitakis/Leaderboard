package com.mpolitakis.leaderboard.hiltContainer

import android.app.Application
import dagger.hilt.android.HiltAndroidApp

@HiltAndroidApp
class BaseApplication : Application(){
}