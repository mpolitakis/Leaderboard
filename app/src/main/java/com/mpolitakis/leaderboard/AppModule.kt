package com.mpolitakis.leaderboard


import com.mpolitakis.leaderboard.networking.ApiService
import com.mpolitakis.leaderboard.repository.LeaderBoardRepository
import com.mpolitakis.leaderboard.repository.LeaderBoardRepositoryImpl
import dagger.Module
import dagger.Provides
import dagger.hilt.InstallIn
import dagger.hilt.components.SingletonComponent


@Module
@InstallIn(SingletonComponent::class)
class AppModule{

    @Provides
    fun provideLeaderBoardRepository(apiService: ApiService) : LeaderBoardRepository =
        LeaderBoardRepositoryImpl(apiService)

}