package com.mpolitakis.leaderboard.networking.response

data class LeaderBoardResponse (
    val page : Int,

    val list : ArrayList<LeaderBoardResponseItem>
)