package com.mpolitakis.leaderboard.networking

import okhttp3.Interceptor

interface ConnectivityInterceptor : Interceptor