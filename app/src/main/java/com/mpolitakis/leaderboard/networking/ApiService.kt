package com.mpolitakis.leaderboard.networking


import com.mpolitakis.leaderboard.networking.response.LeaderBoardResponseItem
import retrofit2.http.GET
import retrofit2.http.Path


interface ApiService {

    @GET("{page}")
    suspend fun getLeaderBoard(@Path("page") page: Int): List<LeaderBoardResponseItem>


}
