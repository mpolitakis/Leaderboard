package com.mpolitakis.leaderboard.networking.response



data class LeaderBoardResponseItem(
    var numberOfInsert : Int,
    val avatar: String,
    val name: String,
    val points: Int,
    val star_asset: String
)